package edu.hartwick.vanessa.ecologicalmomentaryassessment;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Spinner;

public class SpinnerFragment extends EMAFragment{

    Spinner thisSpinner;
    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.spinner_fragment, container, false);

        thisSpinner = (Spinner) view.findViewById(R.id.theSpinner);
        //The contents of the spinner must be loaded right now
        //This would probably be through like i honestly am not sure
        //This is concerning
        return view;
    }
}
