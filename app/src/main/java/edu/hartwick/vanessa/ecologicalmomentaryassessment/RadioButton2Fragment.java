package edu.hartwick.vanessa.ecologicalmomentaryassessment;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RadioButton;
import android.widget.TextView;

public class RadioButton2Fragment extends RadioButtonFragment {

    RadioButton r1;
    RadioButton r2;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState)
    {
        View view = inflater.inflate(R.layout.radio_buttons2_fragment, container, false); //gotta figure this out
        TextView textView = (TextView) view.findViewById(R.id.questionText);
        textView.setText(question);
        r1 = (RadioButton) view.findViewById(R.id.rbutton1);
        r2 = (RadioButton) view.findViewById(R.id.rbutton2);
        return view;
    }

    @Override
    public void saveContents() {
        //This feels like one of the few occasions in which a switch statement would be helpful
        if(r1.isSelected())
        {
            selected = 1;
        }
        if(r2.isSelected()) {
            selected = 2;
        }

        DataWriter.addRButtonData(selected);
    }
}
