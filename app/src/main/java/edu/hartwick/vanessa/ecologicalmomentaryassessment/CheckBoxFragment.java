package edu.hartwick.vanessa.ecologicalmomentaryassessment;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;



public class CheckBoxFragment extends EMAFragment {

    public String text;
    public String titleText;

    private static CheckBox emaCheckBox;
    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.checkbox_fragment, container, false);

         emaCheckBox = (CheckBox) view.findViewById(R.id.theCheckBox);
         emaCheckBox.setText("I haven't done this yet");
        return view;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);

    }

    @Override
    public void saveContents() {
        boolean selected;
        selected = emaCheckBox.isChecked();

        DataWriter.addCheckBoxData(selected);
        //something like, DataWriter.write(title, selected)
    }
}
