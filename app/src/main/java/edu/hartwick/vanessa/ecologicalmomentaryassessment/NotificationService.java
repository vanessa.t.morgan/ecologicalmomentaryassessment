package edu.hartwick.vanessa.ecologicalmomentaryassessment;

import android.annotation.TargetApi;
import android.app.AlarmManager;
import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.os.Build;
import android.os.IBinder;
import android.support.annotation.RequiresApi;
import android.support.v4.app.NotificationCompat;
import android.app.IntentService;
import android.support.annotation.Nullable;
import org.joda.time.*;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Random;
import java.util.concurrent.ThreadLocalRandom;


public abstract class NotificationService extends IntentService {
//this needs to be luanched later as an intent

    ArrayList<TimeSpan> times = new ArrayList<>();

    public NotificationService()
    {
        super("NotificationService");
    }

    public LocalTime randomTime(long start, long end)
    {
        //This will basically pick out a specific time of day for the notification to be scheduled
        LocalTime time = new LocalTime();
        Random ran = new Random();
        long result;
        result = ThreadLocalRandom.current().nextLong(start, end);
        return LocalTime.fromMillisOfDay(result);
    }


    public void readTime()
    {
        //I don't know what this originally was : (
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        return super.onStartCommand(intent, flags, startId);
    }


    @Override
    public void onDestroy() {
        super.onDestroy();
    }

    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    public void makeNotification(Context context, int notificationID,  LocalTime noteTime)
    {
        //This part builds the notification thingy
        //Note, the notification itself does not change dynamically since it will always be the same from day to day
       // NotificationManager nm = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
        NotificationCompat.Builder mbuilder = new NotificationCompat.Builder(context);
        mbuilder.setSmallIcon(R.drawable.ic_launcher_background);
        mbuilder.setContentTitle("EMA Time!");
        mbuilder.setContentText("Please click on this icon");
        mbuilder.setPriority(NotificationCompat.PRIORITY_DEFAULT);
        mbuilder.setColor(Color.MAGENTA);
        mbuilder.setAutoCancel(true);

        //This actually makes the notifications
        Notification notification = mbuilder.build();

        //This will basically make the intents which allows it to load up the ema activity
        Intent intent = new Intent(context, MyNotificationPublisher.class);
        intent.putExtra(MyNotificationPublisher.NOTIFICATION_ID, notificationID);
        intent.putExtra(MyNotificationPublisher.NOTIFICATION, notification);
        PendingIntent activity = PendingIntent.getActivity(context, notificationID, intent, PendingIntent.FLAG_CANCEL_CURRENT);
        //mbuilder.setContentIntent(activity);
        //nm.notify(1, mbuilder.build());
        //This creates a long of the milliseconds between right now and the desired time
        LocalTime now  = LocalTime.now();
        long futureTime;
        futureTime = noteTime.getMillisOfDay() - now.getMillisOfDay();
        AlarmManager alarmManager = (AlarmManager) context.getSystemService(Context.ALARM_SERVICE);
        //This actually sets everyting
        alarmManager.set(AlarmManager.ELAPSED_REALTIME_WAKEUP, futureTime, activity);
    }

}
