package edu.hartwick.vanessa.ecologicalmomentaryassessment;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;

import com.google.gson.Gson;

import java.util.ArrayList;

public final class Global {
    //This class is basically entirely static
    //This means that all variables in this class are shared between all instances of the class


    //I need something to store the times in which people request that they don't get notifications


    private static ArrayList config = new ArrayList(); //I'm not 100% sure what this is supposed to do
    private static int counter; //This is for counting through
    private static ArrayList<TimeSpan> span = new ArrayList<TimeSpan>(); //This is an arraylist of the time spans in which the user can't be contacted
    private static int emaMax; //This is the total number of ema screens
    private static int emaCounter = 0; //This is what is used to check which of the ema screens the app is on
    private static String id; //So this is the id that will be attached to the file for the thing

    public static void makeGlobal(ArrayList conf, int count, ArrayList<TimeSpan> spa, int emaMa){
        //This is the
        config = conf;
        count = count;
        span = spa;
        emaMax = emaMa;
    }

    public void Global(Global glob)
    {
     //     Global.config =
    }

    public static void setID(String identification)
    {
        id = identification;
        //I can't figure out how to make it final in this configuration, but it should only be set once
    }

    public static String getID()
    {
        return id;
    }

    public static int getEmaMax()
    {
        return emaMax;
    }

    public static void setEmaMax(int max)
    {
        emaMax = max;
    }

    public static int getEmaCounter() {return emaCounter;}

    public static void setEmaCounter(int count) {emaCounter = count;}

    public static ArrayList getConfig()
    {
        return config;
    }

    public static void setConfig(ArrayList con)
    {
        config = con;
    }

    public static int getCounter()
    {
        return counter;
    }

    public static void setCounter(int count)
    {
        counter = count;
    }

    public static void incrimenetCounter()
    {
        counter++;
    }

    public static ArrayList getSpan()
    {
        return span;
    }

    public static void setSpan(ArrayList sp)
    {
        span = sp;
    }

    public static void appendSpan(TimeSpan sp)
    {
        span.add(sp);
    }

    public static void removeSpan(TimeSpan timeSpan)
    {
        span.remove(timeSpan);
    }

    public static void saveConfig(Bundle savedInstanceState)
    {

    }

    public static void loadConfig()
    {
        //So this would use the ReadingFile class I made
    }

    public static void addConfig(String addition)
    { //low key, can't remember what this is for
        if(addition.equals("/s"))
        {

        }
        else if(addition.equals("/tb"))
        {

        }
        else if (addition.equals("/rb2"))
        {

        }
        else if (addition.equals("/rb3"))
        {

        }
        else if (addition.equals("/rb4"))
        {

        }
        else if (addition.equals("/rb5"))
        {

        }
        else if (addition.equals("/rb6"))
        {

        }
        else if (addition.equals("/rb7"))
        {

        }
        else if (addition.equals("/s"))
        {

        }
        else
        {
            //something about starting the next thing
        }
    }

    public static void addCheckbox(String text,String title)
    {
        CheckBoxFragment checkboxFrag = new CheckBoxFragment();
        checkboxFrag.text = text;
        checkboxFrag.titleText = title;
        config.add(checkboxFrag);
    }

    public static void addTextBox(String text)
    {
        TextBoxFragment textView = new TextBoxFragment();
        textView.text = text;
        config.add(textView);
    }

    public static void addRB2(String question, String title)
    {
        RadioButton2Fragment rb = new RadioButton2Fragment();
        rb.question = question;
        rb.title = title;
        config.add(rb);
    }

    public static void addRB3(String question, String title)
    {
        RadioButton3Fragment rb = new RadioButton3Fragment();
        rb.question = question;
        rb.title = title;
        config.add(rb);
    }

    public static void addRB4(String question, String title)
    {
        RadioButton4Fragment rb = new RadioButton4Fragment();
        rb.question = question;
        rb.title = title;
        config.add(rb);
    }
    public static void addRB5(String question, String title)
    {
        RadioButton5Fragment rb = new RadioButton5Fragment();
        rb.question = question;
        rb.title = title;
        config.add(rb);
    }
    public static void addRB6(String question, String title)
    {
        RadioButton6Fragment rb = new RadioButton6Fragment();
        rb.question = question;
        rb.title = title;
        config.add(rb);
    }

    public static void addRB7(String question, String title)
    {
        RadioButton7Fragment rb = new RadioButton7Fragment();
        rb.question = question;
        rb.title = title;
        config.add(rb);
    }

    public static void addSpinner(String placeholder)
    {
        //I will figure this out eventually
    }

    public static void appendList()
    {
        //this will append the config list
    }

    public String toString()
    {//This translates the object into a json string so that it can be added to shared preferences
        Gson gson = new Gson();
        return gson.toJson(this);
    }

    public static void fromString(String json)
    {//This takes a string and translates it into the Global
        Gson gson = new Gson();
      //  Global = gson.fromJson(json, Global.class); //I gotta figure this one out : (
            //It used to be like Global glob = ..., but i need it different
            //Maybe i can take this line out
        config = Global.config;
        counter = Global.getCounter();
        span = Global.getSpan();
    }

}
