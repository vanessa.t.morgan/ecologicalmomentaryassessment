
package edu.hartwick.vanessa.ecologicalmomentaryassessment;

import android.content.Context;

import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Scanner;
import java.io.File;

public class ReadingFile {

    /*This class is used primarily to read variables from the embedded text file and then
        basically convert them into variables the rest of the app can utilize
    */
    private Context ctx;
    private String line = null;
    public ArrayList<String> widgetValues = new ArrayList<>();
    boolean eventConting;

    public ReadingFile(Context context)
    {
        ctx = context;
    }

    public void readFile(int config_id)
    {
        // readFile(R.raw.textfile);
        // this would be how i call this method (dunno why i'm putting it here, but i am)
        line = null;
        try
        {
            //opens and input stream pointing to the config file
            InputStream is = ctx.getResources().openRawResource(config_id);

            Scanner reader = new Scanner(new InputStreamReader(is));
            int counter = 0;
            String title;
            String text;
            String type;
            ArrayList<String> stringArray = new ArrayList<String>(); //used for storing values for spinner
            int spinnerCounter;

            eventConting = reader.nextBoolean(); //This chooses if the thing is

            while(reader.hasNext())
            {
                //Each of these reads through the entry and assigns these variables to the thingy
                if(reader.nextLine().equals("/cb"))
                {
                    title = reader.nextLine();
                    text = reader.nextLine();
                    type = "Check Box";
                }
                else if(reader.nextLine().equals("/rb2") || reader.nextLine().equals("/rb3")
                        || reader.nextLine().equals("/rb4") || reader.nextLine().equals("/rb5")
                        || reader.nextLine().equals("/rb6")|| reader.nextLine().equals("/rb7"))
                //I will add regular expressions later
                {
                    title = reader.nextLine();
                    text = reader.nextLine();
                    type = "Radio Buttons"; //this needs looking at
                }
                else if(reader.nextLine().equals("/s")){
                    spinnerCounter = reader.nextInt();
                    title = reader.nextLine();
                    text = reader.nextLine();
                    type = "Spinner";
                    for(int i = 0; i < spinnerCounter; i++)
                    {
                        stringArray.add(reader.nextLine());
                    }
                }

                title = "";
                text = "";
                type = "";

            }

        }
        catch (Exception ex)
        {
            System.out.println("Something went wrong with your file");
        }

        //return FileContents;
    }

}
