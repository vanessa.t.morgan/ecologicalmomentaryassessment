package edu.hartwick.vanessa.ecologicalmomentaryassessment;

import android.app.FragmentTransaction;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.util.AttributeSet;
import android.view.View;
import android.widget.Button;

import com.google.gson.Gson;

import java.util.ArrayList;

public class EMAScreen extends FragmentActivity {
    //This is the screen in which the user inputs data and stuff

    FragmentManager fm;
    EMAFragment frag1;
    EMAFragment frag2;
    EMAFragment frag3;
    private boolean hasFrag1;
    private boolean hasFrag2;
    private boolean hasFrag3;
    private boolean isEnd;
    private Button advance;
    final SharedPreferences globe = getApplicationContext().getSharedPreferences("Globe", 0);
    Gson gson = new Gson();
    Fragment firstFragment;
    Fragment secondFragment;
    Fragment thirdFragment;

   // @androidx.annotation.Nullable
    @Override
    public View onCreateView(String name, Context context, AttributeSet attrs) {
       // View view = inflater.inflate(R.layout.checkbox_fragment, container, false);
     //I'm going to have to set each fragment individually right here I think
     //Don't need for loop

        android.app.FragmentManager fragmentManager = getFragmentManager();
        FragmentTransaction transaction = fragmentManager.beginTransaction();


        if(true) //Placeholder
        {
            //transaction.add(R.id.radioButtons2, firstFragment);
        }

        if(Global.getEmaCounter()==0)
        {
            DataWriter.makeFile(context);
            DataWriter.writeHeader(context, Global.getConfig().toString()); //This ALMOST CERTAINLY has to get changed and stuff
        }

        Global.setEmaCounter(Global.getEmaCounter() + 1); //I think this should work since
        //the global variable should reset at the end of the sequence
        //It is initialized at zero in the class
        //I REALLY hope this works

        setContentView(R.layout.data_activity);
       // advance = (Button) findViewById(R.id.advanceButton);

        if(Global.getEmaCounter() == Global.getEmaMax()) //If the activity is on the last screen
        {//This would be if the thing is at the very last screen
            isEnd = true;
        }
        else //If it's not the last screen
        {
            isEnd = false;
        }

        if(Global.getEmaCounter() == 0) //in the first activity
        { //Loads the global in case it hasn't already
            Global.makeGlobal(gson.fromJson(globe.getString("conf", ""), ArrayList.class),
                    globe.getInt("count", 0),
                    gson.fromJson(globe.getString("span", ""), ArrayList.class),
                    globe.getInt("emaMax", 0));
        }//Low key, I'm not sure how to check if it's the first ema screen in sequence


        //This entire section isn't working
        if(hasFrag1) { //Code only activates if it has this fragment
            if (Global.getConfig().get(Global.getCounter()).equals("/cb")) {
                frag1 = (EMAFragment) getSupportFragmentManager().findFragmentById(R.id.checkBoxFrag);
            } else if (Global.getConfig().get(Global.getCounter()).equals("/rb2")) {
                frag1 = (EMAFragment) getSupportFragmentManager().findFragmentById(R.id.radioButtons2);
            } else if (Global.getConfig().get(Global.getCounter()).equals("/rb3")) {
                frag1 = (EMAFragment) getSupportFragmentManager().findFragmentById(R.id.radioButtons3);
            } else if (Global.getConfig().get(Global.getCounter()).equals("/rb4")) {
                frag1 = (EMAFragment) getSupportFragmentManager().findFragmentById(R.id.radioButtons4);
            } else if (Global.getConfig().get(Global.getCounter()).equals("/rb5")) {
                frag1 = (EMAFragment) getSupportFragmentManager().findFragmentById(R.id.radioButtons5);
            } else if (Global.getConfig().get(Global.getCounter()).equals("/rb6")) {
                frag1 = (EMAFragment) getSupportFragmentManager().findFragmentById(R.id.radioButtons6);
            } else if (Global.getConfig().get(Global.getCounter()).equals("/rb7")) {
                frag1 = (EMAFragment) getSupportFragmentManager().findFragmentById(R.id.radioButtons7);
            } else if (Global.getConfig().get(Global.getCounter()).equals("/tv")) {
                frag1 = (EMAFragment) getSupportFragmentManager().findFragmentById(R.id.textBoxFrag);
            } //Realistically, this piece of code ALWAYS activates
            Global.setCounter(Global.getCounter() + 1);
        }
        if(hasFrag2) //This is the first piece that is actually conditional
        {
            if (Global.getConfig().get(Global.getCounter()).equals("/cb")) {
                frag2 = (EMAFragment) getSupportFragmentManager().findFragmentById(R.id.checkBoxFrag);
            } else if (Global.getConfig().get(Global.getCounter()).equals("/rb2")) {
                frag2 = (EMAFragment) getSupportFragmentManager().findFragmentById(R.id.radioButtons2);
            } else if (Global.getConfig().get(Global.getCounter()).equals("/rb3")) {
                frag2 = (EMAFragment) getSupportFragmentManager().findFragmentById(R.id.radioButtons3);
            } else if (Global.getConfig().get(Global.getCounter()).equals("/rb4")) {
                frag2 = (EMAFragment) getSupportFragmentManager().findFragmentById(R.id.radioButtons4);
            } else if (Global.getConfig().get(Global.getCounter()).equals("/rb5")) {
                frag2 = (EMAFragment) getSupportFragmentManager().findFragmentById(R.id.radioButtons5);
            } else if (Global.getConfig().get(Global.getCounter()).equals("/rb6")) {
                frag2 = (EMAFragment) getSupportFragmentManager().findFragmentById(R.id.radioButtons6);
            } else if (Global.getConfig().get(Global.getCounter()).equals("/rb7")) {
                frag2 = (EMAFragment) getSupportFragmentManager().findFragmentById(R.id.radioButtons7);
            } else if (Global.getConfig().get(Global.getCounter()).equals("/tv")) {
                frag2 = (EMAFragment) getSupportFragmentManager().findFragmentById(R.id.textBoxFrag);
            }
            Global.setCounter(Global.getCounter() + 1);
        }
        if(hasFrag3) //Also conditional
        {
            if (Global.getConfig().get(Global.getCounter()).equals("/cb")) {
                frag3 = (EMAFragment) getSupportFragmentManager().findFragmentById(R.id.checkBoxFrag);
            } else if (Global.getConfig().get(Global.getCounter()).equals("/rb2")) {
                frag3 = (EMAFragment) getSupportFragmentManager().findFragmentById(R.id.radioButtons2);
            } else if (Global.getConfig().get(Global.getCounter()).equals("/rb3")) {
                frag3 = (EMAFragment) getSupportFragmentManager().findFragmentById(R.id.radioButtons3);
            } else if (Global.getConfig().get(Global.getCounter()).equals("/rb4")) {
                frag3 = (EMAFragment) getSupportFragmentManager().findFragmentById(R.id.radioButtons4);
            } else if (Global.getConfig().get(Global.getCounter()).equals("/rb5")) {
                frag3 = (EMAFragment) getSupportFragmentManager().findFragmentById(R.id.radioButtons5);
            } else if (Global.getConfig().get(Global.getCounter()).equals("/rb6")) {
                frag3 = (EMAFragment) getSupportFragmentManager().findFragmentById(R.id.radioButtons6);
            } else if (Global.getConfig().get(Global.getCounter()).equals("/rb7")) {
                frag3 = (EMAFragment) getSupportFragmentManager().findFragmentById(R.id.radioButtons7);
            } else if (Global.getConfig().get(Global.getCounter()).equals("/tv")) {
                frag3 = (EMAFragment) getSupportFragmentManager().findFragmentById(R.id.textBoxFrag);
            }
            Global.setCounter(Global.getCounter() + 1);
        }

     if(isEnd)
     {
         //It will change the text of the button if it's the final ema screen
         advance.setText("Finish");
     }

     /*
        advance.setOnClickListener(//Listens for the button to get clicked
                new Button.OnClickListener() {
                    public void onClick(View v) {
                        advance(v);
                    }

                }
        );

    */ //I'm like 80% sure this does nothing, but I'm still afraid to delete it
        return super.onCreateView(name, context, attrs);
    }

    public void advance(View view, Context context)
    {
        //This method is going to be the method which advances the user to the next activity
        if(hasFrag1)
        {//Will implement the saving stuff when I come to it
            frag1.saveContents();
        }
        if(hasFrag2)
        {
            frag2.saveContents();
        }
        if(hasFrag3)
        {
            frag3.saveContents();
        }

        if(!isEnd)
        {//It is called for every activity but the very last one
            Intent intent = new Intent(this, EMAScreen.class);
            startActivity(intent); //note, this
        }
        else
        {//This should only get called once it's at the last one
            DataWriter.Write(context);
        }
        Global.setEmaCounter(0); //resets the EMA counter for the next time
        finish(); //This should call the destroy method. I'm pretty sure this piece of code will
                    //be called once the very last start activity is finished
    }
}
