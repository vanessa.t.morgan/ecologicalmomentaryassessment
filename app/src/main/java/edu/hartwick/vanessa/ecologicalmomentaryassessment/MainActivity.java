package edu.hartwick.vanessa.ecologicalmomentaryassessment;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.util.AttributeSet;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;

import com.google.gson.Gson;

import org.joda.time.LocalTime;

import java.util.ArrayList;

// import androidx.annotation.Nullable;

public class MainActivity extends Activity {

   // int numberOfTimes; //The amount of time spans that can't be went through

    EditText idCollect;
    EditText timeStart;
    EditText timeEnd;
    Button saveStuff;
    Button deleteStuff;
    Button enterData;
    Spinner times; //A spinner displaying the timespans that the participant doens't wanna be reached
    String idNumber; //personal id number for each participant (must be entered with help of researcher)
    Boolean eventContingent; //This is wether the app will go event contingent or random prompt
    ArrayList<TimeSpan> donttimes = new ArrayList<TimeSpan>(); //The list of times not to disturb
    ArrayList<String> dontStrings = new ArrayList<String>();
    boolean eventContig;

 //   final SharedPreferences globe = getApplicationContext().getSharedPreferences("Globe", 0);
   // final SharedPreferences sp = getApplicationContext().getSharedPreferences("ID", 0);


    @Override
    protected void onCreate( Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }

    @Override
    public View onCreateView(View parent, String name, Context context, AttributeSet attrs) {
        idCollect = (EditText) findViewById(R.id.idCollector);
        timeStart = (EditText) findViewById(R.id.timeStart);
        timeEnd = (EditText) findViewById(R.id.timeEnd);
        saveStuff = (Button) findViewById(R.id.saveButton);
        deleteStuff = (Button) findViewById(R.id.deleteButton);
        times = (Spinner) findViewById(R.id.dontDisturbList);
        enterData = (Button) findViewById(R.id.startDataButton);
        Gson gson = new Gson();

        if(!eventContig)
        {
            enterData.setEnabled(false); //It's disabled
            enterData.setVisibility(View.GONE); //It's invisible
        }
        else
        {
            //This is what will happen to make all this random prompt and stuff q
        }

/*
        Global.makeGlobal(gson.fromJson(globe.getString("conf", ""), ArrayList.class),
                globe.getInt("count", 0),
                gson.fromJson(globe.getString("span", ""), ArrayList.class), globe.getInt("max", 0)); //this is probably the longest method call I've ever made

        //Checks if there is a id value in shared prefererences
        idNumber = sp.getString("id", "");

        if(idNumber.equals("")) //If there is none
        {
            idCollect.setEnabled(true);
            timeStart.setEnabled(false);
            timeEnd.setEnabled(true);
        }
        else //If there is one already
        {
            idCollect.setText(idNumber); //loads the id into the text field
            idCollect.setEnabled(false); //sets the text field so that it can't be edited
        }

        final SharedPreferences spans = getApplicationContext().getSharedPreferences("times", 0);
        numberOfTimes = spans.getInt("counter", 0 ); //this checks how many times there are

        for(int i = 0; i < numberOfTimes; i++)
        {
            //This should get the previous times in the sharedpreferences
            dontStrings.add(spans.getString(String.valueOf(numberOfTimes), ""));
            //I need to look at this way harder soon
        }




        //ArrayAdapter<TimeSpan> adapt = ArrayAdapter.createFromResource(this, (should be the arraylist) , android.R.layout.simple_spinner_item);
        //times.setAdapter(adapt);


        */
        return super.onCreateView(parent, name, context, attrs);
    }

    @Override
    protected void onDestroy() {
    /*    final SharedPreferences.Editor globeEdit = globe.edit();
        Gson gson = new Gson();
        globeEdit.putString("conf", gson.toJson(Global.getConfig()));
        globeEdit.putInt("count", Global.getCounter());
        globeEdit.putString("span", gson.toJson(Global.getSpan()));

        final SharedPreferences.Editor spEditor = sp.edit();
        spEditor.putInt("counter", numberOfTimes); */
        super.onDestroy();
    }

    private void onSaveClick(View view)
    {
        if(!idCollect.isEnabled()) {
            TimeSpan span;
            String startString = timeStart.getText().toString();
            String endString = timeEnd.getText().toString();
            LocalTime start = LocalTime.parse(startString);
            LocalTime end = LocalTime.parse(endString);
            span = new TimeSpan(start, end);
            Global.appendSpan(span);
            //And then added to sharedpreferences
        }


        if(idCollect.isEnabled() && !idCollect.getText().toString().equals(""))
        {//If the idCollect is editable and something is in there
          /*  idNumber = idCollect.getText().toString();
            SharedPreferences.Editor edit = sp.edit();
            edit.putString("id", idNumber);
            edit.commit();

            Toast toast = Toast.makeText(getApplication(), "Your ID number was saved", Toast.LENGTH_LONG);
            toast.show();
            timeStart.setEnabled(true);
            timeEnd.setEnabled(true); */
        }
    }



    public void startData(View view)
    {
        Intent i = new Intent(getApplicationContext(), EMAScreen.class);
        startActivity(i);
    }

    private void deleteClick(View view)
    {
        String current;
        String[] cur;
        TimeSpan span;
        LocalTime start;
        LocalTime end;
        current = times.getSelectedItem().toString();
        cur = current.split(",");
        start = LocalTime.parse(cur[0]);
        end = LocalTime.parse(cur[1]);
        span = new TimeSpan(start, end);

       // SharedPreferences.Editor edit = sp.edit();
        //String spanString;
    }

}
