package edu.hartwick.vanessa.ecologicalmomentaryassessment;

import android.content.Context;

import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.util.Calendar;
import java.util.Scanner;

public final class DataWriter {
    //this class will be used to write to the embedded text file
    private static File toFile; //This is the file used for all the stuff
    private static FileWriter writer;
    private static String entry;

    //Basically, entry is a buffer
        //It is what is written to by most of the stuff
        //It is appended to the file and then wiped

    //For excel (or SPSS or Libreoffice Calc or something) purposes, "," denotes the seperator between cells

    public void DataWriter() {
        //idk why i have this here, but i'm a tiny bit afraid to delete it
        //You know how it is ;)
    }

    public static void makeFile(Context context)
    {
        toFile = new File(context.getFilesDir(), Global.getID() + "Data.txt");
            //I'm pretty sure it sets the file to a sorta internal directory locations?
        //It saves so it's the id and then data.text
            //Maybe I'll find some better name, but maybe not
        if(!toFile.exists())
        {//If the file doesn't already exist, it makes it
            toFile.mkdir();
        }//Ultimately, the if statment is vestigial
    }

    public static void appendEntry()
    {
        //So this class wil basically
        //I legit left off writing that sentence yesterday so i have NO CLUE how it ends
    }

    public static void addRButtonData(int selected) {
        //This if for adding the data of a radio button to the entry
        entry = entry + ", " + String.valueOf(selected);
    }

    public static void addCheckBoxData(boolean check) {
        //This is for adding the data of a checkbox to the entry
        entry = entry + ", " + String.valueOf(check);
    }

    public static String getEntry()
    {
        return entry;
    }


    //I'll add more add stuff once I get more like fragments and stuff

    public static void Write(Context context) {
        //So this one is going to append the actual file with the row of stuff
        //So, it has to print a new line and then print the string it has been given
        try {
            toFile = new File(context.getFilesDir(), Global.getID() + "Data.txt"); //This assigns the file a directory location
            FileWriter writer = new FileWriter(toFile, true); //
            writer.write(System.lineSeparator()); //This is the easiest way I could find to add a new line
            writer.write(entry); //This writes the actual entry of stuff
            writer.flush(); //Flushes the stuff so it's all cleasr and stuff
            writer.close();
        } catch(Exception except)
        {
            except.printStackTrace(); //idk what this does
            //I'll look into it if i get that chance
        }
        entry = ""; //This is used to clear the entry of stuff
    }

    private static boolean checkHeader(Context context) {
        //So this will check to see if the specified string is in the following part of the header for the text file

        String first = "";
        try {
                toFile = new File(context.getFilesDir(),Global.getID() + "Data.txt");
                Scanner scanner = new Scanner(toFile);
                first = scanner.nextLine(); //this should basically assign the first line to scanner
        }
        catch(Exception exception)
        {
            //nothing
        }
        return first.startsWith("Date"); //If the first thing in the first line of the file is "Date"
    }

    public static void addDate() { //This should be the first one called
        //This should be used right when the EMA activities start
        Calendar cal = Calendar.getInstance();
        int day;
        int month;
        day = cal.get(Calendar.MONTH);
        month = cal.get(Calendar.DAY_OF_MONTH);
        String date = String.valueOf(day) + "/" + String.valueOf(month);
        //I am for this time excluding year because i can't imagine it would be that big of a thing
        entry = entry + date + ", ";
    }

    public static void addTime() { //This should be the second one called
        //This should be called immediately after addDate
        Calendar cal = Calendar.getInstance();
        int hour;
        int minute;
        int second;
        hour = cal.get(Calendar.HOUR);
        minute = cal.get(Calendar.MINUTE);
        second = cal.get(Calendar.SECOND);
        String time = String.valueOf(hour) + ":" + String.valueOf(minute) + ":" + String.valueOf(second);
        entry = entry + time + ", ";
    }

    private static String getPath() {
        //I don't remember why this is here and I regret not documenting what the heck it is before I made it
        return "placeholder";
    }

    public static void writeHeader(Context context, String titles) {
        //So this tries to write the header for the data file
        //This shoudl only be called once and then never again
        try {
        toFile = new File(context.getFilesDir(), Global.getID() + "Data.txt");
        FileWriter writer = new FileWriter(toFile); //
        writer.append("Date, Time, " + titles.toString());
        writer.flush();
        writer.close();
    } catch(Exception except)
        {
            except.printStackTrace(); //idk what this does
            //I'll look into it if i get that chance
        }
    }

    private static String formatTitles(String[] titles)
    {
        //So, this basically formats the entries of titles into a string and stuff
        String result = "";
        for(int i = 0; i<titles.length; i++)
        {
            result = result + ", " + titles[i].toString();
        }
        return result;
        //It basically adds a comma (as that is the seperator
    }
}
