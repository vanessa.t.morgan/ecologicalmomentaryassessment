package edu.hartwick.vanessa.ecologicalmomentaryassessment;

import java.text.SimpleDateFormat;
import java.util.Timer;
import org.joda.time.*;

public class TimeSpan {

    //This class is used to denote a span of time

    private LocalTime start = new LocalTime();
    private LocalTime end = new LocalTime();
    private long span;

    public TimeSpan(LocalTime start, LocalTime end)
    {
        this.start = start;
        this.end = end;
        this.setSpan();
    }

    public void setStart(LocalTime st)
    {
        this.start = st;
    }

    public LocalTime getStart()
    {
        return this.start;
    }

    public void setEnd(LocalTime en)
    {
        this.end = en;
    }

    public LocalTime getEnd()
    {
        return this.end;
    }

    public void setSpan()
    {
        span = end.getMillisOfDay() - start.getMillisOfDay();
    }

    public long getSpan()
    {
        return this.span;
    }

    public String toString()
    {
        return start.toString() + " - " + end.toString();
    }

    public boolean between(LocalTime time)
    {
        return time.isAfter(start) && time.isBefore(end);
    }
}
