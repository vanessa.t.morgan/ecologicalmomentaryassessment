package edu.hartwick.vanessa.ecologicalmomentaryassessment;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RadioButton;
import android.widget.TextView;

public class RadioButton5Fragment extends RadioButtonFragment {

    RadioButton r1;
    RadioButton r2;
    RadioButton r3;
    RadioButton r4;
    RadioButton r5;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState)
    {
        View view = inflater.inflate(R.layout.radio_buttons5_fragment, container, false); //gotta figure this out
        TextView textView = (TextView) view.findViewById(R.id.questionText);
        textView.setText(question);
        r1 = (RadioButton) view.findViewById(R.id.rbutton1);
        r2 = (RadioButton) view.findViewById(R.id.rbutton2);
        r2 = (RadioButton) view.findViewById(R.id.rbutton3);
        r4 = (RadioButton) view.findViewById(R.id.rbutton4);
        r5 = (RadioButton) view.findViewById(R.id.rbutton5);
        return view;
    }

    @Override
    public void saveContents() {
        if(r1.isSelected())
        {
            selected = 1;
        }
        else if(r2.isSelected())
        {
            selected = 2;
        }
        else if(r3.isSelected())
        {
            selected = 3;
        }
        else if(r4.isSelected())
        {
            selected = 4;
        }
        else if(r5.isSelected())
        {
            selected = 5;
        }

        DataWriter.addRButtonData(selected);
    }
}
